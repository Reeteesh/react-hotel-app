import React, { useContext } from 'react';
import { RoomContext } from "../RoomContext";
import Loading from "./Loading";
import Room from "./Room";
import Title from "./Title";


const FeaturedRooms = () => {

    const context = useContext(RoomContext);
    const { greeting, name, featuredRooms, loading } = context;
    console.log(featuredRooms);

    const displayRooms = featuredRooms.map(room => {
        return <Room key={room.id} room={room} />
    });
    return (
        <div>
            <section className="featured-rooms">
                <Title title="featured rooms" />
                <div className="featured-rooms-center">
                    {loading ? <Loading /> : displayRooms}
                </div>
                {/* FeaturedRooms
            {greeting}
                {name} */}
            </section>

        </div>
    )
}

export default FeaturedRooms
