import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../images/logo.svg';
import { FaTimes, FaBars } from "react-icons/fa";


const Navbar = () => {
    const [clicked, setClicked] = useState(false);
    const handleClick = () => {
        setClicked(!clicked)
    }
    return (
        <div className="navbar">
            <div className="nav-center">
                <div className="nav-header">


                    <Link to='/'>
                        <img src={logo} alt="imagelogo" />
                    </Link>

                    <button type='button' onClick={handleClick} className="nav-btn">
                        {clicked ? <FaTimes className='nav-icon' /> : <FaBars className='nav-icon' />}
                    </button>
                </div>
                <ul className={clicked ? 'nav-menu active' : 'nav-menu'}>
                    <li>
                        <Link to='/'>Home</Link>
                    </li>
                    <li>
                        <Link to='/rooms'>Room</Link>
                    </li>
                </ul>
            </div >
        </div >
    )
}

export default Navbar
