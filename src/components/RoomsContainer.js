import React, { useContext } from 'react';
import RoomList from "./RoomList";
import RoomFilter from "./RoomFilter";
import Loading from "./Loading";
import { RoomContext } from '../RoomContext';



const RoomsContainer = () => {
    const { loading, rooms, sortedRooms } = useContext(RoomContext);

    console.log(loading);
    if (loading) {
        return <Loading />
    }
    return (
        <div>
            RoomsContainer
            <RoomFilter rooms={rooms} />
            <RoomList rooms={sortedRooms} />
        </div>
    )
}

export default RoomsContainer
