import React, { useContext, useState } from 'react'
import defaultBcg from "../images/room-1.jpeg";
import Hero from "../components/Hero";
import Banner from "../components/Banner";
import { Link } from "react-router-dom";
import { RoomContext } from "../RoomContext";
import StyledHero from "../components/StyledHero";


const SingleRoom = (props) => {
    console.log(props);
    const [slug, setSlug] = useState(props.match.params.slug);
    console.log(slug);

    const context = useContext(RoomContext);
    const { getRoom } = context;
    console.log(getRoom);

    const room = getRoom(slug);
    console.log(room);
    if (!room) {
        return <div className="error"><h3>Such Rooms could not be found</h3>
            <Link to="/rooms" className="btn-primary">Back to Room</Link></div>
    }

    const {
        name,
        description,
        capacity,
        size,
        price,
        extras,
        breakfast,
        pets,
        images
    } = room;
    const [mainImg, ...otherRemainingImages] = images;
    console.log(otherRemainingImages);
    return (
        <>
            <StyledHero img={images[0] || defaultBcg}>
                <Banner title={`${name} room`}>
                    <Link to="/rooms" className="btn-primary">
                        back to rooms
            </Link>
                </Banner>
            </StyledHero>
            <section className="single-room">
                <div className="single-room-images">
                    {otherRemainingImages.map((item, index) => {
                        return <img key={index} src={item} alt={name} />
                    })}
                </div>
                <div className="single-room-info">
                    <article className="desc">
                        <h3>details</h3>
                        <p>{description}</p>
                    </article>
                    <article className="info">
                        <h3>info</h3>
                        <h6>price : Rs {price}</h6>
                        <h6>size : {size} SQFT</h6>
                        <h6> max capacity :
                {capacity > 1 ? `${capacity} people` : `${capacity} person`}
                        </h6>
                        <h6>{pets ? "pets allowed" : "no pets allowed"}</h6>
                        <h6>{breakfast && "free breakfast included"}</h6>
                    </article>
                </div>
            </section>
            <section className="room-extras">
                <h6>extras </h6>
                <ul className="extras">
                    {extras.map((item, index) => (
                        <li key={index}>- {item}</li>
                    ))}
                </ul>
            </section>
        </>
    )
}

export default SingleRoom
